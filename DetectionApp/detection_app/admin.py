from django.contrib import admin
from.models import Packet, DetectionTool, PacketFlag


class PacketAdmin(admin.ModelAdmin):
    pass


class PacketFlagAdmin(admin.ModelAdmin):
    pass


class DetectionToolAdmin(admin.ModelAdmin):
    pass


admin.site.register(Packet, PacketAdmin)
admin.site.register(PacketFlag, PacketFlagAdmin)
admin.site.register(DetectionTool, DetectionToolAdmin)
