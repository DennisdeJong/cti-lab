from django.db import models



# Create your models here.
class Packet(models.Model):
    src_ip = models.CharField(default='0.0.0.0', max_length=30)
    dst_ip = models.CharField(default='0.0.0.0', max_length=30)
    src_port = models.CharField(default='0', max_length=30)
    dst_port = models.CharField(default='0', max_length=30)
    content = models.CharField(default='', max_length=1000)
    rcv_date = models.DateTimeField(auto_now_add=True)
    checked = models.BooleanField(default=False)
    flagged = models.BooleanField(default=False)

    class Meta:
        get_latest_by = 'rcv_date'
        ordering = ['rcv_date']

    def format_string(self):
        variable_string = {"src_ip": self.src_ip, "dst_ip": self.dst_ip, "src_port": self.src_port, "dst_port": self.dst_port}
        return variable_string

    def __str__(self):
        return self.src_ip+":"+self.src_port+"  =>  "+self.dst_ip+":"+self.dst_port


class PacketFlag(models.Model):
    packet_id = models.ForeignKey(Packet, on_delete=models.CASCADE)
    flag_type = models.IntegerField(null=True, default=0, blank=True)
    flagged_reason = models.CharField(null=True, default='', max_length=200, blank=True)


class DetectionTool(models.Model):
    id = models.IntegerField(primary_key=True)
    tool_name = models.CharField(default="detection tool", max_length=30)
    whitelistable = models.BooleanField(default=False)

    def __str__(self):
        return self.tool_name


class AccessList(models.Model):
    detection_tool_id = models.ForeignKey(DetectionTool, on_delete=models.CASCADE)
    access_list_item = models.CharField(default='0.0.0.0', max_length=30)

    class Meta:
        ordering = ['access_list_item']
        unique_together = (("detection_tool_id", "access_list_item"),)
