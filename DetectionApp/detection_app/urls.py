from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^detectiontool/$', views.detection_tool_overview, name='detectiontool_overview'),
    url(r'^detectiontool/(?P<detectiontool_id>[0-9]+)/$', views.detectiontool, name='detectiontool'),
    url(r'^detectiontool/(?P<detectiontool_id>[0-9]+)/whitelist/(?P<operation>.+)/$', views.detection_tool_access_list_change, name='whitelist'),
]
