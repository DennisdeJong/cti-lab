from django.shortcuts import get_object_or_404, render, redirect
from .models import Packet, DetectionTool, PacketFlag, AccessList
from .forms import WhitelistChanceForm


def index(request):
    latest_packets_list = reversed(Packet.objects.order_by('-rcv_date')[:10])
    latest_flagged_packets_list = reversed(PacketFlag.objects.order_by('-packet_id__rcv_date')[:10])
    detection_tool_list = DetectionTool.objects.all()
    detection_tool_name_list = {}
    for tool in detection_tool_list:
        detection_tool_name_list[tool.id] = tool.tool_name

    context = {'latest_packets_list': latest_packets_list,
               'latest_flagged_packets_list': latest_flagged_packets_list,
               'detection_tool_list': detection_tool_name_list}
    return render(request, 'detection_app/index.html', context)


def detectiontool(request, detectiontool_id):
    form = WhitelistChanceForm()
    tool = get_object_or_404(DetectionTool, pk=detectiontool_id)
    latest_flagged_packets_list = reversed(
        PacketFlag.objects.filter(flag_type=detectiontool_id).order_by('-packet_id__rcv_date')[:10])
    access_list = AccessList.objects.filter(detection_tool_id_id=detectiontool_id).order_by('access_list_item')

    return render(request, "detection_app/detectiontool.html",
                  {'detection_tool': tool, 'latest_flagged': latest_flagged_packets_list, 'whitelist': access_list,
                   'form': form})


def detection_tool_access_list_change(request, detectiontool_id, operation):
    if operation == 'add':
        tool = get_object_or_404(DetectionTool, pk=detectiontool_id)
        AccessList.objects.get_or_create(
            detection_tool_id=tool,
            access_list_item=request.POST['access_list_item'],
        )
    elif operation == 'remove':
        try:
            access_list_item = AccessList.objects.get(detection_tool_id_id=detectiontool_id, access_list_item=request.POST['access_list_item'])
            access_list_item.delete()
        except AccessList.DoesNotExist:
            pass
        except AccessList.MultipleObjectsReturned:
            pass

    return redirect('detection_app:detectiontool', detectiontool_id=detectiontool_id)


def detection_tool_overview(request):
    return render(request, "detection_app/detection_tool_overview.html")
