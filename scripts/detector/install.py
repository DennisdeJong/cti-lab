#!/usr/bin/python3
import os
import sys
os.chdir(os.path.dirname(sys.argv[0]))

# Fix the hostname and remove sudo timeout
os.system("sudo hostnamectl set-hostname detector")
os.system("sed -i 's/^127\.0\.1\.1.*/127.0.1.1\tdetector/' /etc/hosts")
os.system("sudo apt -y update")

# Install dependencies
os.system("sudo apt -y install geoip-bin dnsmasq openssh-server nginx default-libmysqlclient-dev python3-pip vim netcat tcpdump")
os.system("sudo pip3 install virtualenv")
os.system("sudo mkdir ../../DetectionApp/venv")
os.system("sudo virtualenv ../../DetectionApp/venv/")
os.system("sudo ../../DetectionApp/venv/bin/pip3 install -r ../../DetectionApp/requirements.txt")
os.system("sudo mkdir ../../DetectionEngine/venv")
os.system("sudo virtualenv ../../DetectionEngine/venv/")
os.system("sudo ../../DetectionEngine/venv/bin/pip3 install -r ../../DetectionEngine/requirements.txt")
os.system("sudo apt install -y python3-scapy python3-mysql.connector mysql-server")

# Configure external services

## IRC
os.system("git clone https://github.com/eloydegen/miniircd")
os.system("sudo cp -R ./miniircd /opt")
os.system("sudo cp ./irc.service /etc/systemd/system/")
os.system("sudo systemctl start irc")
os.system("sudo systemctl enable irc")

## Webserver
os.system("systemctl enable nginx") # Enable start at boot

## OpenSSH
# No custom configuration needed, it listens on all interfaces by default

## DNS
# For adding more custom DNS zones, modify the dnsmasq.hosts 
os.system("sudo cp ./dnsmasq.conf /etc/dnsmasq.conf")
os.system("sudo cp ./resolv.conf /etc")
os.system("sudo cp ./dnsmasq.hosts /etc/")

# Configure systemd unit file for start script
os.system("sudo cp ./migrate.service /etc/systemd/system/")
os.system("sudo cp ./detection-app.service /etc/systemd/system/")
os.system("sudo cp ./detection-engine.service /etc/systemd/system/")
os.system("sudo cp ./traffic.service /etc/systemd/system/")

# Move preconfigured network settings to system folder
os.system("sudo cp ./network.conf /etc/network/interfaces")
os.system("sudo systemctl restart networking")

# Start the MySQL server
os.system("sudo systemctl start mysql")
os.system("sudo systemctl enable mysql")

# Make database
os.system("sudo mysql -u root -e 'CREATE DATABASE detection'")
os.system("sudo mysql -u root -e 'CREATE DATABASE generation'")
os.system("""sudo mysql -u root -e "CREATE USER 'django'@'localhost' IDENTIFIED BY 'YY7vS6rZ6dmXk24t'" """)
os.system("""sudo mysql -u root -e "CREATE USER 'django'@'10.88.1.2' IDENTIFIED BY 'YY7vS6rZ6dmXk24t'" """)
os.system("""sudo mysql -u root -e "GRANT ALL PRIVILEGES ON detection.* TO 'django'@'localhost'" """)
os.system("""sudo mysql -u root -e "GRANT ALL PRIVILEGES ON generation.* TO 'django'@'10.88.1.2'" """)
os.system("""sudo mysql -u root -e "FLUSH PRIVILEGES" """)

# Setup django database and start server
os.system("sudo ../../DetectionApp/venv/bin/python3 ../../DetectionApp/manage.py makemigrations")
os.system("sudo ../../DetectionApp/venv/bin/python3 ../../DetectionApp/manage.py migrate")
os.system('''echo "from django.contrib.auth import get_user_model; User = get_user_model(
);User.objects.create_superuser('django', '15126471@student.hhs.nl', 'YY7vS6rZ6dmXk24t')" | ../../DetectionApp/venv/bin/python3 ../../DetectionApp/manage.py shell''')

# Populate Django database
os.system("""sudo mysql -u root -e "INSERT INTO detection.detection_app_detectiontool 
            (id, tool_name, whitelistable) VALUES
            (1, 'DNS Tool', 0),
            (2, 'Foreign Countries Tool', 0),
            (3, 'SSH Tool', 1);" """)

# Move traffic starting and traffic detection files to /opt
os.system("sudo cp ./start.py /opt")       
os.system("sudo cp ./traffic.py /opt")
os.system("sudo cp -R ../../DetectionApp /opt")
os.system("sudo cp -R ../../DetectionEngine /opt")
os.system("sudo chmod +x /opt/traffic.py")
os.system("sudo chmod +x /opt/start.py")

# Enable all systemd units to start at boot and start them now
os.system("sudo systemctl enable migrate")
os.system("sudo systemctl enable detection-engine")
os.system("sudo systemctl enable detection-app")
os.system("sudo systemctl enable traffic")
os.system("sudo systemctl start migrate")
os.system("sudo systemctl start detection-engine")
os.system("sudo systemctl start detection-app")
os.system("sudo systemctl start traffic")

# Probably not needed, but reload to be sure
os.system("systemctl daemon-reload")
