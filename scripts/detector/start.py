#!/usr/bin/python3
import os
import sys

os.chdir(os.path.dirname(sys.argv[0]))

# Enable IP routing
os.system("sudo sh -c 'echo 1 > /proc/sys/net/ipv4/ip_forward'")

# Run migrations, in case some model has changed
os.system("sudo /opt/DetectionApp/venv/bin/python3 opt/DetectionApp/manage.py makemigrations")
os.system("sudo /opt/DetectionApp/venv/bin/python3 /opt/DetectionApp/manage.py migrate")