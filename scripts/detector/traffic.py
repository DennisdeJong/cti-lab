#!/usr/bin/python3
from datetime import datetime

from scapy.all import *
import mysql.connector

# MySQL credentials
hostname = 'localhost'
username = 'django'
password = 'YY7vS6rZ6dmXk24t'
database = 'detection'

# Setup connection
connection = mysql.connector.connect(host='localhost', database='detection', user='django', password='YY7vS6rZ6dmXk24t')

def insert_db(pkt):
    if IP in pkt:
        ip_src=pkt[IP].src
        ip_dst=pkt[IP].dst
    if TCP in pkt:
        tcp_sport=pkt[TCP].sport
        tcp_dport=pkt[TCP].dport
        cursor = connection.cursor()
        current_date = datetime.now()
        format_string = {
            'ip_src': str(ip_src),
            'ip_dst': str(ip_dst),
            'src_port': str(tcp_sport),
            'dst_port': str(tcp_dport),
            'content': "abc",
            'rcv_date': current_date.strftime('%Y-%m-%d %H:%M:%S'),
        }
        query = '''INSERT INTO `detection_app_packet` (`src_ip`, `dst_ip`, `src_port`, `dst_port`, `content`, `rcv_date`) VALUES ("{ip_src}","{ip_dst}","{src_port}","{dst_port}","{content}","{rcv_date}");'''
	# Prevent traffic from database queries to pullute the database
        if tcp_dport != 3306 and tcp_sport != 3306 and tcp_dport != 8000 and tcp_sport != 8000:
                result = cursor.execute(query.format(**format_string))
                connection.commit()
                print(" IP src " + str(ip_src) + " TCP sport " + str(tcp_sport)) 
                print(" IP dst " + str(ip_dst) + " TCP dport " + str(tcp_dport))
    if UDP in pkt:
        udp_sport=pkt[UDP].sport
        udp_dport=pkt[UDP].dport
        cursor = connection.cursor()
        current_date = datetime.now()
        format_string = {
            'ip_src': str(ip_src),
            'ip_dst': str(ip_dst),
            'src_port': str(udp_sport),
            'dst_port': str(udp_dport),
            'content': "abc",
            'rcv_date': current_date.strftime('%Y-%m-%d %H:%M:%S'),
        }
        query = '''INSERT INTO `detection_app_packet` (`src_ip`, `dst_ip`, `src_port`, `dst_port`, `content`, `rcv_date`) VALUES ("{ip_src}","{ip_dst}","{src_port}","{dst_port}","{content}","{rcv_date}");'''
        # Filter only DNS traffic
        if udp_dport == 53:
            result = cursor.execute(query.format(**format_string))
            connection.commit()
            print(" IP src " + str(ip_src) + " UDP sport " + str(udp_sport)) 
            print(" IP dst " + str(ip_dst) + " UDP dport " + str(udp_dport))

sniff(iface="enp0s3", filter="ip",prn=insert_db, store=0)

