#!/usr/bin/python3
import os
import sys
os.chdir(os.path.dirname(sys.argv[0]))

# Get internet on the external interface
os.system("sudo dhclient enp0s8")

# Set hostname 
os.system("sudo hostnamectl set-hostname generator")
# Fix the sudo timeout with changed hostname
os.system("sed -i 's/^127\.0\.1\.1.*/127.0.1.1\tgenerator/' /etc/hosts")
os.system("sudo apt -y update")

# Install Python dependencies
os.system("sudo apt -y install default-libmysqlclient-dev vim python3-pip mysql-server netcat tcpdump")
os.system("sudo pip3 install virtualenv")
os.system("sudo mkdir ../../GenerationApp/venv")
os.system("sudo virtualenv ../../GenerationApp/venv/")
os.system("sudo ../../GenerationApp/venv/bin/pip3 install -r ../../GenerationApp/requirements.txt")

# Install database
os.system("sudo systemctl start mysql")
os.system("sudo systemctl enable mysql")

# Make database
os.system("sudo mysql -u root -e 'CREATE DATABASE generation'")
os.system("""sudo mysql -u root -e "CREATE USER 'django'@'localhost' IDENTIFIED BY 'YY7vS6rZ6dmXk24t'" """)
os.system("""sudo mysql -u root -e "GRANT ALL PRIVILEGES ON generation.* TO 'django'@'localhost'" """)
os.system("""sudo mysql -u root -e "FLUSH PRIVILEGES" """)

# Setup django database and start server
os.system("sudo ../../GenerationApp/venv/bin/python3 ../../GenerationApp/manage.py makemigrations")
os.system("sudo ../../GenerationApp/venv/bin/python3 ../../GenerationApp/manage.py migrate")
os.system('''echo "from django.contrib.auth import get_user_model; User = get_user_model();User.objects.create_superuser('django', '15126471@student.hhs.nl', 'YY7vS6rZ6dmXk24t')" | ../../GenerationApp/venv/bin/python3 ../../GenerationApp/manage.py shell''')

# Configure systemd unit file
os.system("sudo cp ./start.py /opt/")
os.system("sudo cp -R ../../GenerationApp /opt/")
os.system("sudo chmod +x /opt/start.py")

os.system("sudo cp ./migrate.service /etc/systemd/system")
os.system("sudo cp ./generation-app.service /etc/systemd/system")

# Install requirements for the commandline GenerationPackage and copy to /opt
os.system("pip3 install -r ../../GenerationPackage/requirements.txt")
os.system("sudo cp -R ../../GenerationPackage/ /opt")

# Move network configuration to system folder
os.system("sudo cp ./network.conf /etc/network/interfaces")
os.system("sudo systemctl restart networking")

# Enable and start services
os.system("sudo systemctl enable migrate")
os.system("sudo systemctl enable generation-app")

os.system("sudo systemctl start migrate")
os.system("sudo systemctl start generation-app")

# Disable unessecary DNS lookups
os.system("sudo systemctl stop systemd-timesyncd")
os.system("sudo systemctl disable systemd-timesyncd")