import os

# Delete default route and add new one, so packages can be installed
# After finishing the installation, run start.py again
os.system("sudo ip r del default && sudo dhclient -r && sudo dhclient")