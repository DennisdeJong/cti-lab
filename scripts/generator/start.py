#!/usr/bin/python3
import os

# Remove DHCP default route and add new one
os.system("sudo ip route del default && sudo ip route add default via 10.88.0.1")

os.system("sudo /opt/GenerationApp/venv/bin/python3 /opt/GenerationApp/manage.py makemigrations")
os.system("sudo /opt/GenerationApp/venv/bin/python3 /opt/GenerationApp/manage.py migrate")