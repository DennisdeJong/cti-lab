DATABASES = {
        'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'detection',
        'USER': 'django',
        'PASSWORD': 'YY7vS6rZ6dmXk24t',
        'HOST': 'localhost',   # Or an IP Address that your DB is hosted on
        'PORT': '3306',
        }
}

# ForeignCountriesRule
FOREIGN_COUNTRIES_RULE_LIST = ['KP', 'PRK', 'RU', 'RUS' 'CHN', 'CN']
FOREIGN_COUNTRIES_RULE_REASON = "Connection was made to suspicious destination: {dst_country}"

# SSHRule
SSH_RULE_REASON = "SSH Connection was made to destination: {dst_ip}, which is not on the whitelist"

# DNSRule
DNS_RULE_LIST = ['192.168.1.2', '192.168.1.3']
DNS_RULE_REASON = "A DNS request was made with DNS-server: {dst_ip}, which is not on the allowed DNS-server list"

GET_PACKETS_QUERY = '''SELECT * FROM detection.detection_app_packet WHERE checked = 0 LIMIT 50;'''
UPDATE_PACKETS_QUERY = '''UPDATE detection.detection_app_packet SET checked = {checked}, flagged = {flagged} WHERE id={id};'''
INSERT_PACKET_FLAG_QUERY = '''INSERT INTO detection.detection_app_packetflag (flag_type, flagged_reason, packet_id_id) VALUES ({flag_type}, "{reason}", {id});'''
SELECT_ACCESS_LIST_QUERY = '''SELECT access_list_item FROM detection.detection_app_accesslist WHERE detection_tool_id_id = {id} LIMIT 10000;'''
