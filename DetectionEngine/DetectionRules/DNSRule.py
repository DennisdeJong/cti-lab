from DetectionRules.DetectionRule import DetectionRule
from Packet import Packet
from settings import DNS_RULE_LIST, DNS_RULE_REASON


class DNSRule(DetectionRule):

    def __init__(self, rule_id):
        self._whitelist = DNS_RULE_LIST
        self._reason = DNS_RULE_REASON
        super().__init__(rule_id)

    def check(self, packet: Packet):
        if not packet.dst_port == '53':
            return packet, None

        found_in_whitelist = False
        for dns in self._whitelist:
            if packet.dst_ip == dns:
                found_in_whitelist = True
                break
        if not found_in_whitelist:
            return super().flag_packet(packet, self._reason.format(dst_ip=packet.dst_ip))
        return packet, None
