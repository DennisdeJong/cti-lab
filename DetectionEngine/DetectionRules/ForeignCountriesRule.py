from DetectionRules.DetectionRule import DetectionRule
from Packet import Packet
from settings import FOREIGN_COUNTRIES_RULE_LIST, FOREIGN_COUNTRIES_RULE_REASON
import subprocess


class ForeignCountriesRule(DetectionRule):

    def __init__(self, rule_id):
        self._blacklist = FOREIGN_COUNTRIES_RULE_LIST
        self._reason = FOREIGN_COUNTRIES_RULE_REASON
        super().__init__(rule_id)

    def check(self, packet: Packet):
        packet_flag = None
        country = subprocess.check_output(['geoiplookup', packet.dst_ip]).decode('ascii')[23:].split(",")[0]
        for c in self._blacklist:
            if country == c:
                packet, packet_flag = super().flag_packet(packet, self._reason.format(dst_country=country))
        return packet, packet_flag

