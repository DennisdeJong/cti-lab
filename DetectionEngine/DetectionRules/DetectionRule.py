from Packet import Packet
from PacketFlag import PacketFlag
from database import get_connection
from settings import SELECT_ACCESS_LIST_QUERY
from MySQLdb import DatabaseError


class DetectionRule:

    def __init__(self, rule_id):
        self._rule_id = rule_id # Same ID as the DetectionTool ID from the view

    def check(self, packet: Packet):
        return packet, None

    def flag_packet(self, packet, reason):
        packet.flagged = 1
        packet_flag = PacketFlag(packet.packet_id, self._rule_id, reason)
        print("Flagged: "+packet_flag.flag_reason)
        return packet, packet_flag

    def get_list(self):
        cursor = get_connection().cursor()
        try:
            cursor.execute(SELECT_ACCESS_LIST_QUERY.format(id=self._rule_id))
            result = cursor.fetchall()
            return result
        except DatabaseError as e:
            print(str(e))

    @property
    def rule_id(self):
        return self._rule_id
