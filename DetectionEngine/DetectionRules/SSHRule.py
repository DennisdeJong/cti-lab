from DetectionRules.DetectionRule import DetectionRule
from Packet import Packet
from settings import SSH_RULE_REASON


class SSHRule(DetectionRule):

    def __init__(self, rule_id):
        self._reason = SSH_RULE_REASON
        super().__init__(rule_id)

    def check(self, packet: Packet):
        if not packet.dst_port == '22':
            return packet, None

        found_in_whitelist = False
        for ip in super().get_list():
            if packet.dst_ip != ip[0]:
                continue
            else:
                found_in_whitelist = True
                break
        if not found_in_whitelist:
            return super().flag_packet(packet, self._reason.format(dst_ip=packet.dst_ip))
        return packet, None
