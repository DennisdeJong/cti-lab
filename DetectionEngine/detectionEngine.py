from time import sleep
from MySQLdb import DatabaseError
from DetectionRules.DNSRule import DNSRule
from DetectionRules.ForeignCountriesRule import ForeignCountriesRule
from Packet import Packet
from DetectionRules.SSHRule import SSHRule
from database import get_connection
from settings import GET_PACKETS_QUERY, UPDATE_PACKETS_QUERY, INSERT_PACKET_FLAG_QUERY


class DetectionEngine:
    def __init__(self):
        self.cursor = get_connection().cursor()
        dns_rule = DNSRule(1)
        foreign_countries_rule = ForeignCountriesRule(2)
        ssh_rule = SSHRule(3)
        self.detection_rule_list = []
        self.detection_rule_list.append(dns_rule)
        self.detection_rule_list.append(foreign_countries_rule)
        self.detection_rule_list.append(ssh_rule)

    def run(self):
        packet_list = self.get_packets()
        flag_list = []
        for packet in packet_list:
            for rule in self.detection_rule_list:
                packet, packet_flag = rule.check(packet)
                if packet_flag:
                    flag_list.append(packet_flag)
            packet.checked = 1
        self.update_packets(packet_list, flag_list)

    def get_packets(self):
        try:
            self.cursor.execute(GET_PACKETS_QUERY)
            result = self.cursor.fetchall()

        except DatabaseError as e:
            print(str(e))
        packet_list = []
        for item in result:
            packet = Packet(item[0], item[1], item[2], item[3], item[4], item[5], item[6], item[7], item[8])
            packet_list.append(packet)

        return packet_list

    def update_packets(self, packet_list, flag_list):
        for packet in packet_list:
            packet_info = {
                'id': packet.packet_id,
                'checked': packet.checked,
                'flagged': packet.flagged
            }
            try:
                query = UPDATE_PACKETS_QUERY.format(**packet_info)
                self.cursor.execute(query)
            except DatabaseError as e:
                print(str(e))
        for flag in flag_list:
            flag_info = {
                'id': flag.packet_id,
                'flag_type': flag.flag_type,
                'reason': flag.flag_reason
            }
            try:
                query = INSERT_PACKET_FLAG_QUERY.format(**flag_info)
                self.cursor.execute(query)
            except DatabaseError as e:
                print(str(e))
        get_connection().commit()


if __name__ == "__main__":
    de = DetectionEngine()
    while 1:
        de.run()
        sleep(3)
