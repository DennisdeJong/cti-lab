import MySQLdb
import settings

_connection = None


def get_connection():
    global _connection
    if not _connection:
        _connection = MySQLdb.connect(host=settings.DATABASES['default']['HOST'],
                                      user=settings.DATABASES['default']['USER'],
                                      passwd=settings.DATABASES['default']['PASSWORD'],
                                      db=settings.DATABASES['default']['NAME'])
        _connection.time_zone = '+00:00'
    return _connection
