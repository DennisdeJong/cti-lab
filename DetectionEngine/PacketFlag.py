class PacketFlag(object):
    def __init__(self, packet_id, flag_type, flag_reason):
        self._packet_id = packet_id
        self._flag_type = flag_type
        self._flag_reason = flag_reason

    @property
    def packet_id(self):
        return self._packet_id

    @property
    def flag_type(self):
        return self._flag_type

    @flag_type.setter
    def flag_type(self, value):
        self._flag_type = value

    @property
    def flag_reason(self):
        return self._flag_reason

    @flag_reason.setter
    def flag_reason(self, value):
        self._flag_reason = value
