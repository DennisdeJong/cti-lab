class Packet(object):
    def __init__(self, packet_id, src_ip, dst_ip, src_port, dst_port, content, rcv_date, checked, flagged):
        self._packet_id = packet_id
        self._src_ip = src_ip
        self._dst_ip = dst_ip
        self._src_port = src_port
        self._dst_port = dst_port
        self._content = content
        self._rcv_date = rcv_date
        self._checked = checked
        self._flagged = flagged

    def __str__(self):
        string = self._src_ip+":"+self._src_port+"  =>  "+self._dst_ip+":"+self._dst_port+"| Checked: "+str(self._checked)+"| Flagged: "+str(self._flagged)+" | Flagged by: "+str(self.flag_type)
        return string

    @property
    def packet_id(self):
        return self._packet_id

    @property
    def src_ip(self):
        return self._src_ip

    @property
    def dst_ip(self):
        return self._dst_ip

    @property
    def src_port(self):
        return self._src_port

    @property
    def dst_port(self):
        return self._dst_port

    @property
    def rcv_date(self):
        return self._rcv_date

    @property
    def checked(self):
        return self._checked

    @checked.setter
    def checked(self, value):
        self._checked = value

    @property
    def flagged(self):
        return self._flagged

    @flagged.setter
    def flagged(self, value):
        self._flagged = value


