How to make a scenario

1. Choose which IoC to use and make new instances of them

2. Make IoCThread instances for each IoC

3. Make a list of the IoCThread instances and make a second list with each interval

4. Make an instance of the Scenario class and give the lists as parameters.

5. Run scenario