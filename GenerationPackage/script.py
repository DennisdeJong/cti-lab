from IOCGenerator.thread import IoCThread
from IOCGenerator.scenario import Scenario
from IOCGenerator.DNSIoC import DNSIoC
from IOCGenerator.DoSIoC import DoSIoC

# Step 1
dns_ioc = DNSIoC()
foreign_ioc = DoSIoC()

# Step 2
dns_thread = IoCThread(name='dns', iocgenerator=dns_ioc, interval=1)
dos_thread = IoCThread(name='dos', iocgenerator=foreign_ioc, interval=1)

# Step 3
thread_list = [dns_thread, dos_thread]
interval_list = [[3, 10, 5], [13, 5, 10]]

# Step 4
botnet_scenario = Scenario(thread_list, interval_list)
botnet_scenario.run()



