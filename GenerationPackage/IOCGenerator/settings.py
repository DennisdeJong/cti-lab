# IoC traffic generator settings 

# DNSIoC
dns_ips = ['77.77.77.77']
dns_dst = "google.com"

# DOSIoC
dos_host = "8.199.123.1"

# ForeignCountryIoC
foreign_ips = ["5.16.0.0", "176.111.208.1", "185.10.172.0"]

# IRCIoC
irc_server = '165.254.255.133'
irc_port = 6667
irc_nickname = 'test_py'
irc_channel = '#test_py'

# SSHIoC
ssh_nbytes = 4096
ssh_hostname = '66.66.66.66'
ssh_port = 22
ssh_username = 'user'
ssh_password = 'user'
ssh_command = 'whoami'

# SYNIoC
syn_host = "51.15.40.196"
amount_packets = 10

# TrafficGenerator
traffic_websites = ["google.nl", "youtube.nl", "nu.nl"]
