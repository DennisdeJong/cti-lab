from IOCGenerator import settings
from .IoCGenerator import IoCGenerator
from scapy.all import *
from scapy.layers.inet import IP, TCP
import random


class SYNIoC(IoCGenerator):

    def run(self):
        for x in range(0, settings.amount_packets):
            IP_Packet = IP()
            IP_Packet.src = self.randIP()  # Use randomized source address, so this machine won't SYN-ACK it
            IP_Packet.dst = settings.syn_host  # Loopback interface of the external VM

            TCP_Packet = TCP()
            TCP_Packet.sport = random.randint(2000, 5000)
            TCP_Packet.dport = 22  # SYN to open port
            TCP_Packet.flags = "S"
            TCP_Packet.seq = random.randint(2000, 5000)
            TCP_Packet.window = random.randint(2000, 5000)
            send(IP_Packet / TCP_Packet, verbose=0)

    # Generate random IP address
    def randIP(self):
        ip_addr = "10.88.0."
        random_num = str(random.randint(100, 254))
        final_ip = ip_addr + random_num
        return final_ip
