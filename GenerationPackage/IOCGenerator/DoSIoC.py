from IOCGenerator import settings
from .IoCGenerator import IoCGenerator
import os


class DoSIoC(IoCGenerator):

    def run(self):
        # Get hostname to ping from settings.py
        hostname = settings.dos_host

        # Execute shell command
        response = os.system("ping " + hostname + " -l 65500 -n 5")

        # Print response from ping
        print(response)


