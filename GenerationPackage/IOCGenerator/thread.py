import threading
import time
from IOCGenerator import IoCGenerator


class IoCThread(threading.Thread):
    def __init__(self, name, interval, iocgenerator: IoCGenerator):
        self._ioc_generator = iocgenerator
        self._status = 0
        self._stop = 0
        self._interval = interval
        super().__init__(name=name)

    def run(self):
        print("run thread:", self.name)
        self._status = 0
        while not self._stop:
            while not (not self._status or self._stop):
                self._ioc_generator.run()
                time.sleep(self._interval)
        print('Killed thread: ', self.name)

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, s):
        self._status = s

    @property
    def stop(self):
        return self._stop

    @stop.setter
    def stop(self, s):
        self._stop = s


