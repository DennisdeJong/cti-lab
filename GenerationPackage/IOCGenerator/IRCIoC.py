from IOCGenerator import settings
from .IoCGenerator import IoCGenerator
import socket


class IRCIoC(IoCGenerator):

    def run(self):
        # Open a socket to handle the connection
        irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Open a connection with the server
        def irc_conn():
            irc.connect((settings.irc_server, settings.irc_port))

        # Trying to connect to an IRC server by executing the function
        irc_conn()


