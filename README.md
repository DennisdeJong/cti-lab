# CTI Lab
This is the lab source code for Cyber Threat Intelligence for the Hague University of Applied Sciences

Note: The first network adapter will be enp0s3 and the second enp0s8

## Deployment
## Generator
1st adapter:
*  type: internal
*  name: generator-detector

2nd adapter:
* type: bridged
```
sudo dhclient enp0s8
sudo apt update
sudo apt install git
git clone https://gitlab.com/DennisdeJong/cti-lab
cd cti-lab/scripts/generator/
chmod +x ./install.py
./install.py
```
The web interface can be used on port 8000 of the `enp0s8` interface. The IP adres can be found with the following command: `ip address show dev enp0s8`
### Detector
1st adapter:
* type: internal
* name: generator-detector

2nd adapter: 
* type: bridged
```
sudo dhclient enp0s8
sudo apt update
sudo apt install git
git clone https://gitlab.com/DennisdeJong/cti-lab
cd cti-lab/scripts/detector/
chmod +x ./install.py
./install.py
```
The web interface can be used on port 8000 of the `enp0s9` interface. The IP adres can be found with the following command: `ip address show dev enp0s9`

## Usage
The `GenerationPackage` on the generator is available in `/opt/GenerationPackage`. The dependencies are specified in requirements.txt and are automatically installed the first time. 

To start the application, run `sudo python3 ./script.py`

When you add new dependencies, run `pip3 install -r requirements.txt` inside this folder.


