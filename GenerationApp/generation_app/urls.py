from django.conf.urls import url
from django.urls import path
from .views import ScenarioPageView
from . import views

urlpatterns = [
    path('scenario/', ScenarioPageView.as_view(), name='scenario'),
    url(r'^$', views.index, name='index'),
    url(r'^start/$', views.start, name='start'),
    url(r'^stop/$', views.stop, name='stop'),
]