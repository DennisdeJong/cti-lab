from django.shortcuts import render, redirect
import threading
from generation_app.IOCGenerator.thread import IoCThread
from . import settings as s
from django.views.generic import TemplateView


class ScenarioPageView(TemplateView):
    template_name = 'GenerationApp/scenario.html'


# function rendering requested index page of generation app
def index(request):
    return render(request, 'GenerationApp/index.html')


# start requested IoC thread if it exists
def start(request):
    ioc_gen_id = request.POST['ioc_gen_id']
    if ioc_gen_id != '':
        ioc_gen = s.ioc_dict.get(ioc_gen_id, '')
        if ioc_gen != '':
            thread = IoCThread(name=ioc_gen_id, iocgenerator=ioc_gen, interval=3)
            thread.start()
            thread.status = 1
        else:
            print('Requested IoC doesn\'t exists.')
    else:
        print('Empty request')
    return redirect('generation_app:index')


# if there are no active threads, redirect to index page right away
# otherwise stop the requested IoC_thread and its status if it exists
def stop(request):
    active_thread_list = threading.enumerate()
    empty_thread_list = len(active_thread_list) <= 0
    if not empty_thread_list:
        ioc_gen_id = request.POST['ioc_gen_id']
        if ioc_gen_id != '':
            for t in active_thread_list:
                if t.getName() == ioc_gen_id:
                    t.status = 0
                    t.stop = 1
        else:
            print('Empty input')

    return redirect('generation_app:index')

