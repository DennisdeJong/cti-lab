from generation_app.IOCGenerator import settings
from .IoCGenerator import IoCGenerator
import paramiko
import warnings
warnings.filterwarnings(action='ignore', module='.*paramiko.*')


class SSHIoC(IoCGenerator):

    def run(self):

        # Create client with settings: hostname and port
        client = paramiko.Transport((settings.ssh_hostname, settings.ssh_port))

        # Try to connect using username and password from settings.py
        client.connect(username=settings.ssh_username, password=settings.ssh_password)

        stdout_data = []
        stderr_data = []

        # Opening session
        session = client.open_channel(kind='session')

        # Executing a command on remote server from the settings.py file ( LS )
        session.exec_command(settings.ssh_command)
        while True:
            if session.recv_ready():
                stdout_data.append(session.recv(settings.ssh_nbytes))
            if session.recv_stderr_ready():
                stderr_data.append(session.recv_stderr(settings.ssh_nbytes))
            if session.exit_status_ready():
                break

        # Print response from server
        print(stdout_data)

        # Finally close session and client
        session.close()
        client.close()

