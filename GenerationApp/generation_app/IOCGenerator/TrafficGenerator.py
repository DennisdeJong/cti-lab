from generation_app.IOCGenerator import settings
from .IoCGenerator import IoCGenerator
import requests


class TrafficGenerator(IoCGenerator):

    def run(self):
        # print("Ik ben een traffic generator")

        for x in settings.traffic_websites:
            print("Visiting: " + x)
            r = requests.get("http://" + x)
            print("Status code: " + str(r.status_code))
            print("Time elapsed: " + str(r.elapsed))
