from generation_app.IOCGenerator import settings
from .IoCGenerator import IoCGenerator
import dns.resolver


class DNSIoC(IoCGenerator):

    def run(self):

        # Create a resolver client
        resolver = dns.resolver.Resolver()

        # Set the nameserver to the nameserver in settings.py ( 8.8.8.8)
        resolver.nameservers = settings.dns_ips

        # Query the dns-server with the domain found in settings.py ( google.com )
        answers = resolver.query(settings.dns_dst, "A")

        # Finally iterate answers and print out result
        for rdata in answers:
            print(rdata)


