from generation_app.IOCGenerator import settings
from .IoCGenerator import IoCGenerator
import requests


class ForeignCountryIoC(IoCGenerator):

    def run(self):

        # Iterates the list of IP's and visits it
        for x in settings.foreign_ips:
            try:
                print("Visiting: " + x)
                r = requests.get("http://" + x)
                print("Status code: " + str(r.status_code))
                print("Time elapsed: " + str(r.elapsed))
        # Print exception when no valid response is given back
            except:
                print("*Exception: No valid response*")
