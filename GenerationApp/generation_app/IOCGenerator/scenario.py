from datetime import datetime
from datetime import timedelta


class Scenario:
    def __init__(self, ioc_threads, intervals):
        self._ioc_threads = ioc_threads
        self._intervals = intervals  # List of the intervals of the different IoC's
        self._start_time = []
        self._state = []
        self._running = 1

    def run(self):
        for ioc_thread in self._ioc_threads:
            ioc_thread.start()
            self._start_time.append(datetime.now())
            self._state.append(0)
        while self._running:
            i = 0
            for ioc_thread in self._ioc_threads:
                if datetime.now() > self._start_time[i] + timedelta(seconds=self._intervals[i][self._state[i]]):
                    if not ioc_thread.status:
                        print("Starting Thread ", ioc_thread.name)
                        ioc_thread.status = 1
                        self._state[i] = 1
                        self._start_time[i] = datetime.now()
                    else:
                        print("Stopping Thread ", ioc_thread.name)
                        ioc_thread.status = 0
                        self._state[i] = 0
                        self._start_time[i] = datetime.now()
                i += 1

    def stop_scenario(self):
        for ioc_thread in self._ioc_threads:
            ioc_thread.stop = 1
        self._running = 0
