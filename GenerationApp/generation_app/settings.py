
from .IOCGenerator import TrafficGenerator,ForeignCountryIoC,DNSIoC,SSHIoC,DoSIoC,IRCIoC

ioc_dict = {
    '1': TrafficGenerator.TrafficGenerator(),
    '2': ForeignCountryIoC.ForeignCountryIoC(),
    '3': DNSIoC.DNSIoC(),
    '4': SSHIoC.SSHIoC(),
    '5': DoSIoC.DoSIoC(),
    '6': IRCIoC.IRCIoC(),
}
